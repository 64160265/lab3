public class Problem16 {
    public static void main(String[] args) {
        // Prints upper half pattern
        for (int i = 1; i <= 5; i++) {
            // Prints i spaces at the beginning of each row
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            // Prints i to rows value at the end of each row
            for (int j = i; j <= 5; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}
